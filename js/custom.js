$(document).ready(function() {
	$(document).on('click', '#download', function(e) {
		e.preventDefault();

		window.location.href = $(this).data('base-url') + 'download';
	});
});
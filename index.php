<?php
require_once 'vendor/autoload.php';
require_once 'app/helpers/tracker.php';
require_once 'app/helpers/util.php';

use \Slim\App;
use \Slim\Http\Stream;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$util = new Util();
$tracker = new Tracker();

# Create the Slim application
$app = new App();

# Get the application container to add dependencies to
$container = $app->getContainer();

# Add the ability to use views
$container['view'] = new \Slim\Views\PhpRenderer('views/');

# Create the frontend routes
$app->get('/', function(Request $request, Response $response, array $args) use ($util, $tracker) {
	$response = $this->view->render($response, 'home.php', [
		'base_url' => $util->base_url()
	]);

	return $response;
})->setName('home');

$app->get('/download', function(Request $request, Response $response, array $args) use ($util, $tracker) {
	$file = __DIR__ . '/downloads/labour_hours.json';
	file_put_contents($file, json_encode($tracker->data(), JSON_PRETTY_PRINT));

	$handler = fopen($file, 'rb');
	$stream = new Stream($handler);

	return $response->withHeader('Content-Type', 'application/force-download')
		->withHeader('Content-Type', 'application/octet-stream')
		->withHeader('Content-Type', 'application/download')
		->withHeader('Content-Description', 'File Transfer')
		->withHeader('Content-Transfer-Encoding', 'binary')
		->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
		->withHeader('Expires', '0')
		->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
		->withHeader('Pragma', 'public')
		->withBody($stream);
});

# Run the Slim application
$app->run();
?>
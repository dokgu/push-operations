<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Time Tracker - Aperture Laboratories</title>
		<meta name="description" content="">
		<meta charset="utf-8">
		<meta name="description" content="Time Tracker - Aperture Laboratories" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Meta data for when sharing link -->
		<meta property="og:title" content="Time Tracker - Aperture Laboratories" />
		<meta property="og:description" content="Time Tracker - Aperture Laboratories" />
		<meta property="og:type" content="website" />
		<meta property="og:locale" content="en_CA" />
		<meta property="og:locale:alternate" content="en_US" />
		<link rel="icon" href="<?php echo $base_url; ?>images/favicon.ico" />
		<!-- Google Font: Lato -->
		<link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<!-- Bootstrap -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/bootstrap/bootstrap-docs.css?v=1.0.0">
		<!-- FontAwesome -->
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/font-awesome/css/all.min.css?v=1.0.0">
		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/custom.css?v=1.0.0">
		<!-- jQuery -->
		<script src="<?php echo $base_url; ?>js/jquery.min.js?v=1.0.0"></script>
		<!-- Custom Script -->
		<script src="<?php echo $base_url; ?>js/custom.js?v=1.0.2"></script>
	</head>
	<body>
		<div class="container vertical-middle">
			<div class="text-center">
				<h2>Aperture Laboratories</h2>

				<p class="context">Download the JSON file containing the number of hours each shift per day for each employee.</p>
				<button type="button" id="download" class="btn btn-primary" data-base-url="<?php echo $base_url; ?>">Download JSON</button>
			</div>
		</div>
		<!-- Bootstrap -->
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
	</body>
</html>
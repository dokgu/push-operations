<?php
class Util {
	/**
	 * Gets a preformatted string of the variable dump.
	 * Uses output buffer to prevent errors.
	 *
	 * Very useful when the request was made with AJAX so you can dump the
	 * variable and return the contents to the script.
	 */
	public function dump_var($variable) {
		ob_start();
		echo "<pre style='text-align: left; white-space: pre-wrap;'>";
		var_dump($variable);
		echo "</pre>";
		$result = ob_get_contents();
		ob_end_clean();

		return $result;
	}

	/**
	 * Get the base URL for the application.
	 */
	public function base_url() {
		# Get the base path.
		$server_name = $_SERVER['SERVER_NAME'];
		$server_port = $_SERVER['SERVER_PORT'];
		$dir = dirname(dirname(dirname(__FILE__)));
		$dir = str_replace('\\', '/', $dir); # Only for Windows servers
		$dir = str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir);
		$path = $server_port == "80" ? "//{$server_name}{$dir}/" : "//{$server_name}:{$server_port}{$dir}/";

		return $path;
	}
}
?>
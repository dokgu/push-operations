<?php
require_once 'util.php';

class Tracker {
	/**
	 * Morning    => 05:00:00 - 11:59:59
	 * Afternoon  => 12:00:00 - 17:59:59
	 * Evening    => 18:00:00 - 22:59:59
	 * Late Night => 23:00:00 - 04:59:59
	 */
	const PERIODS = array(
		'period1' => array(
			'start' => '05:00:00',
			'end'   => '11:59:59'
		),
		'period2' => array(
			'start' => '12:00:00',
			'end'   => '17:59:59'
		),
		'period3' => array(
			'start' => '18:00:00',
			'end'   => '22:59:59'
		),
		'period4' => array(
			'start' => '23:00:00',
			'end'   => '04:59:59'
		)
	);
	const DECIMALS = 1;

	var $employees = array();
	var $util = null;

	public function __construct() {
		$this->util = new Util();
	}

	public function data() {
		$string = file_get_contents(dirname(dirname(dirname(__FILE__))) . '/data/data.json');
		$data = json_decode($string, true);

		# Employee information
		foreach($data['employees'] as $employee) {
			$this->employees[$employee['id']] = array(
				'employee_id' => $employee['id'],
				'first_name'  => $employee['first_name'],
				'last_name'   => $employee['last_name'],
				'labour'      => array()
			);
		}

		# Clocked time
		foreach($data['clocks'] as $shift) {
			$in = DateTime::createFromFormat('Y-m-d H:i:s', $shift['clock_in_datetime']);
			$out = DateTime::createFromFormat('Y-m-d H:i:s', $shift['clock_out_datetime']);

			$this->set_shifts($shift['employee_id'], $in, $out);
		}

		# Round the total hours per day
		foreach($this->employees as $i => $employee) {
			foreach($employee['labour'] as $j => $arr) {
				$this->employees[$i]['labour'][$j]['total'] = round($arr['total'], self::DECIMALS);
			}
		}

		return $this->employees;
	}

	/**
	 * @param $period [ 'period1' | 'period2' | 'period3' | 'period4' ]
	 */
	private function shifts($employee_id, $in, $out, $period) {
		if(in_array(strtolower($period), array('period1', 'period2', 'period3', 'period4'))) {
			# Start with the date the employee clocked in
			$current_day = clone $in;
			$next_day = clone $in;
			$next_day->modify('+1 day');

			$current_period = self::PERIODS[$period];

			# Adjust the dates if we're looking at late night shifts
			if(strtolower($period) == 'period4') {
				if(intval($in->format('G')) >= 23) {
					# Clocked in between 11pm - midnight
				} else if(intval($in->format('G')) < 5) {
					# Clocked in between midnight - 5am
					$current_day->modify('-1 day');
					$next_day->modify('-1 day');
				}
			}

			while(true) {
				$end_day = strtolower($period) == 'period4' ? $next_day : $current_day;

				$start = DateTime::createFromFormat('Y-m-d H:i:s', "{$current_day->format('Y-m-d')} {$current_period['start']}");
				$end = DateTime::createFromFormat('Y-m-d H:i:s', "{$end_day->format('Y-m-d')} {$current_period['end']}");

				# Check if the period falls within the timeframe
				if(($start >= $in || $end <= $out) || ($start <= $in && $end >= $out)) {
					# Within the timeframe
					$times = $this->get_start_and_end_times($in, $out, $start, $end);

					if(!empty($times)) {
						# Get the hours for each day
						$hours = $this->hours($times['start_time'], $times['end_time']);

						foreach($hours as $date => $value) {
							# Find the index of the date
							$index = null;

							if(!empty($this->employees[$employee_id]['labour'])) {
								foreach($this->employees[$employee_id]['labour'] as $i => $labour) {
									if($labour['date'] == $date) {
										$index = $i;
										break;
									}
								}
							}

							if(is_null($index)) {
								# Add a new date
								$data = array(
									'date'                  => $date,
									'total'                 => 0,
									'labour_by_time_period' => array(
										'period1' => 0,
										'period2' => 0,
										'period3' => 0,
										'period4' => 0
									)
								);

								$data['total'] += $value;
								$data['labour_by_time_period'][strtolower($period)] += $value;

								$this->employees[$employee_id]['labour'][] = $data;
							} else {
								# Date exists, merge the data
								$this->employees[$employee_id]['labour'][$index]['total'] += $value;
								$this->employees[$employee_id]['labour'][$index]['labour_by_time_period'][strtolower($period)] += $value;
							}
						}

						# Check the next day
						$current_day->modify('+1 day');
						$next_day->modify('+1 day');
					} else {
						break;
					}
				} else {
					# Outside of the timeframe - we stop here
					break;
				}
			}
		}
	}

	private function set_shifts($employee_id, $in, $out) {
		foreach(self::PERIODS as $period => $arr) {
			$this->shifts($employee_id, $in, $out, $period);
		}
	}

	private function get_start_and_end_times($in, $out, $start, $end) {
		if(($in >= $start && $in <= $end) || ($out >= $start && $out <= $end) || ($start >= $in && $end <= $out) || ($in >= $start && $out <= $end)) {
			$start_time = null;
			$end_time = null;

			if($in >= $start && $in <= $end) {
				# Shift started within the period
				$start_time = $in;
			} else {
				# Shift started before the period
				$start_time = $start;
			}

			if($out >= $start && $out <= $end) {
				# Shift ended within the period
				$end_time = $out;
			} else {
				# Shift ended after the period
				$end_time = $end;
			}

			return array(
				'start_time' => $start_time,
				'end_time'   => $end_time
			);
		}

		return null;
	}

	private function hours($start, $end) {
		if($start->format('d') == $end->format('d')) {
			return array(
				$start->format('Y-m-d') => round(abs($end->getTimestamp() - $start->getTimestamp()) / 60 / 60, self::DECIMALS)
			);
		} else {
			# Start to midnight
			$midnight_current = DateTime::createFromFormat('Y-m-d H:i:s', "{$start->format('Y-m-d')} 23:59:59");

			# Midnight to end
			$midnight_next = DateTime::createFromFormat('Y-m-d H:i:s', "{$end->format('Y-m-d')} 00:00:00");

			return array(
				$start->format('Y-m-d') => round(abs($midnight_current->getTimestamp() - $start->getTimestamp()) / 60 / 60, self::DECIMALS),
				$end->format('Y-m-d')   => round(abs($end->getTimestamp() - $midnight_next->getTimestamp()) / 60 / 60, self::DECIMALS)
			);
		}
	}
}
?>
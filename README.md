## Requirements

[![php](https://img.shields.io/badge/PHP-7.4-blue.svg)](https://gitlab.com/dokgu/push-operations)

This repository requires the use of [Composer](https://getcomposer.org/).

## Cloning Instructions

1. `cd /var/www/html/` - go to your Apache web server's root directory
2. `git clone git@gitlab.com:dokgu/push-operations.git` - clone the repository
3. `cd push-operations` - go to the project folder
4. `composer install` - install all project dependencies
5. Open your web browser and go to `localhost/push-operations`
6. Click <kbd>Download JSON</kbd> to generate the output file and download a local copy

## Notes

- The main logic to process the data is located in [`app/helpers/tracker.php`](https://gitlab.com/dokgu/push-operations/-/blob/master/app/helpers/tracker.php).
- The `labour_hours.json` file will only be generated when you click the <kbd>Download JSON</kbd> button and the file will be located in the `downloads` folder.